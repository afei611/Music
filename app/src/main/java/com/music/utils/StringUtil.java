package com.music.utils;

import android.text.TextUtils;

import java.util.List;

/**
 * Created by dingfeng on 2016/8/2.
 */
public class StringUtil {

    public static boolean isEmpty(String s) {
        if (!TextUtils.isEmpty(s) && s != null) {
            return false;
        }
        return true;
    }

    // String列表转换成用逗号隔开的字符串
    public static String formatStringList(List<String> list) {
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < list.size(); i++) {
            if (i == 0) {
                sb.append(list.get(i).trim());
            }else {
                sb.append("," + list.get(i).trim());
            }
        }
        return sb.toString();
    }

    public static String getSubStr(String str, int num) {
        String result = "";
        int i = 0;
        while (i < num) {
            int lastFirst = str.lastIndexOf('/');
            result = str.substring(lastFirst) + result;
            str = str.substring(0, lastFirst);
            i++;
        }
        return result.substring(1);
    }

    public static String makeTimeString(long milliSecs) {
        StringBuffer sb = new StringBuffer();
        long m = milliSecs / (60 * 1000);
        sb.append(m < 10 ? "0" + m : m);
        sb.append(":");
        long s = (milliSecs % (60 * 1000)) / 1000;
        sb.append(s < 10 ? "0" + s : s);
        return sb.toString();
    }

    public static String secondTo(int second, boolean isChinese) {
        int h = 0;
        int d = 0;
        int s = 0;
        int temp = second % 3600;
        if (second > 3600) {
            h = second / 3600;
            if (temp != 0) {
                if (temp > 60) {
                    d = temp / 60;
                    if (temp % 60 != 0) {
                        s = temp % 60;
                    }
                } else {
                    s = temp;
                }
            }
        } else {
            d = second / 60;
            if (second % 60 != 0) {
                s = second % 60;
            }
        }
        if (isChinese) {
            return h + "小时" + d + "分" + s + "秒";
        } else {
            return h + ":" + d + ":" + s;
        }
    }

}
