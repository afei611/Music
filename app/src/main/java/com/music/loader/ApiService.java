package com.music.loader;

import org.json.JSONObject;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by dingfeng on 2016/9/21.
 */
public interface ApiService {

    @GET(Api.BASE_URL)
    Call<JSONObject> getBillboard(@QueryMap Map<String, Object> options);

    @GET(Api.BASE_URL)
    Call<JSONObject> getBoardMusicList(@QueryMap Map<String, Object> options);

    @GET(Api.BASE_URL)
    Call<JSONObject> getOnlineUri(@QueryMap Map<String, Object> options);

    @GET(Api.BASE_URL)
    Call<JSONObject> getArtistInfo(@QueryMap Map<String, Object> options);

    @GET(Api.BASE_URL)
    Call<JSONObject> getSearchResult(@QueryMap Map<String, Object> options);

    @GET(Api.BASE_URL)
    Call<JSONObject> getLrc(@QueryMap Map<String, Object> options);

}
