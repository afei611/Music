package com.music.ui.view.utils;

/**
 * Created by dingfeng on 2016/9/27.
 */
public enum StateEnum {
    LOADING,// 加载中
    LOAD_SUCCESS,// 加载成功
    LOAD_FAIL,// 加载失败
    EMPTY
}
