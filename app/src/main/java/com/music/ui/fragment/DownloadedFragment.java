package com.music.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.lzy.okserver.download.DownloadInfo;
import com.lzy.okserver.download.DownloadManager;
import com.lzy.okserver.download.DownloadService;
import com.music.R;
import com.music.application.Constants;
import com.music.entity.MusicInfo;
import com.music.entity.online.OnlineMusicInfo;
import com.music.service.MusicUtil;
import com.music.utils.CacheUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dingfeng on 2016/9/23.
 */
public class DownloadedFragment extends FragmentBase {

    private Context mContext;
    private ListView mListView;
    private TextView emptyView;
    private List<DownloadInfo> allTask;
    private List<DownloadInfo> downloadedTask = new ArrayList<>();
    private MyAdapter adapter;
    private DownloadManager downloadManager;
    private CacheUtil mCache;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_downloaded, container, false);
        initView(view);
        mCache = CacheUtil.getInstance(mContext);
        return view;
    }

    private void initView(View view) {
        mListView = (ListView) view.findViewById(R.id.listView);
        mContext = getContext();
        downloadManager = DownloadService.getDownloadManager();
        allTask = downloadManager.getAllTask();
        for (DownloadInfo info : allTask) {
            if (info.getState() == DownloadManager.FINISH) {
                downloadedTask.add(info);
            }
        }
        adapter = new MyAdapter();
        mListView.setAdapter(adapter);
        mListView.setOnItemClickListener(onItemClickListener);
        emptyView = (TextView) view.findViewById(R.id.emptyView);
        mListView.setEmptyView(emptyView);
    }

    AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            DownloadInfo downloadInfo = downloadedTask.get(position);
            OnlineMusicInfo onlineMusicInfo = (OnlineMusicInfo) mCache.getObject(downloadInfo.getUrl());
            String path = downloadInfo.getTargetPath();
            final MusicInfo music = new MusicInfo();
            if (onlineMusicInfo != null) {
                music.setMusicName(onlineMusicInfo.getTitle());
                music.setArtist(onlineMusicInfo.getArtistName());
                music.setCover(onlineMusicInfo.getPicSmall());
                music.setData(path);
            } else {
                music.setMusicName(downloadInfo.getFileName());
                music.setData(path);
            }
            try {
                MusicUtil.sService.playOnline(music);
            } catch (RemoteException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    };

    private class MyAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return downloadedTask.size();
        }

        @Override
        public DownloadInfo getItem(int position) {
            return downloadedTask.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            DownloadInfo downloadInfo = getItem(position);
            OnlineMusicInfo onlineMusicInfo = (OnlineMusicInfo) mCache.getObject(downloadInfo.getUrl());
            ViewHolder viewholder;
            if (convertView == null) {
                viewholder = new ViewHolder();
                convertView = LayoutInflater.from(mContext).inflate(R.layout.item_online_music, null);
                viewholder.iv_cover = (ImageView) convertView.findViewById(R.id.iv_cover);
                viewholder.tv_music_name = (TextView) convertView.findViewById(R.id.tv_music_name);
                viewholder.tv_artist_name = (TextView) convertView.findViewById(R.id.tv_artist_name);
                viewholder.iv_more = (ImageView) convertView.findViewById(R.id.iv_more);
                viewholder.rlMore = (RelativeLayout) convertView.findViewById(R.id.rlMore);
                convertView.setTag(viewholder);
            } else {
                viewholder = (ViewHolder) convertView.getTag();
            }
            viewholder.rlMore.setVisibility(View.GONE);
            if (onlineMusicInfo != null) {
                Glide.with(mContext)
                        .load(onlineMusicInfo.getPicSmall())
                        .placeholder(R.drawable.default_cover)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(viewholder.iv_cover);
                viewholder.tv_music_name.setText(onlineMusicInfo.getArtistName());
                viewholder.tv_artist_name.setText(onlineMusicInfo.getTitle());
            } else {
                viewholder.tv_music_name.setText(downloadInfo.getFileName().replace(Constants.SUFFIX_MP3, ""));
            }

            return convertView;
        }
    }

    class ViewHolder {
        ImageView iv_cover;
        TextView tv_music_name;
        TextView tv_artist_name;
        ImageView iv_more;
        RelativeLayout rlMore;
    }

}
