package com.music.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.music.R;
import com.music.entity.BoardListInfo;
import com.music.ui.activity.BoardMusicListActivity;
import com.music.ui.adapter.BoardListAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dingfeng on 2016/9/21.
 */
public class BoardListFragment extends FragmentBase {

    private Context mContext;
    private List<BoardListInfo> mBoardList = new ArrayList<>();
    private ListView mListView;
    private BoardListAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_board_list, container, false);
        init(view);
        bindData();
        return view;
    }

    private void init(View view) {
        mContext = getContext();
        mListView = (ListView) view.findViewById(R.id.listView);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(mContext, BoardMusicListActivity.class);
                intent.putExtra("board_type", mBoardList.get(position));
                mContext.startActivity(intent);
            }
        });
    }

    private void bindData() {
        if (mBoardList.isEmpty()) {
            String[] types = getResources().getStringArray(R.array.online_board_type);
            for (int i = 0; i < types.length; i++) {
                BoardListInfo info = new BoardListInfo();
                info.setType(types[i]);
                mBoardList.add(info);
            }
        }
        mAdapter = new BoardListAdapter(getContext(), mBoardList);
        mListView.setAdapter(mAdapter);
    }

}
