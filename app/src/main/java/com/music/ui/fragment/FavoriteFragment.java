package com.music.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.music.R;
import com.music.ui.adapter.MusicListAdapter;
import com.music.entity.MusicInfo;
import com.music.sqlite.DBUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dingfeng on 2016/4/28.
 */
public class FavoriteFragment extends FragmentBase
        implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    private TextView txtTitle;
    private ListView mListView;
    private TextView emptyView;

    private MusicListAdapter mAdapter;
    private List<MusicInfo> mMusicList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.favorite_list, container, false);
        initView(view);
        bindData();
        return view;
    }

    private void initView(View view) {
        view.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        txtTitle = (TextView) view.findViewById(R.id.txtTitle);
        txtTitle.setText(R.string.favorite_list);
        mListView = (ListView) view.findViewById(R.id.listView);
        emptyView = (TextView) view.findViewById(R.id.emptyView);
        mListView.setEmptyView(emptyView);
    }

    private void bindData() {
        List<MusicInfo> list = DBUtil.getInstance().getMusicInfoDao().queryList("isFavorite", true);
        if (list != null && list.size() > 0) {
            mMusicList.addAll(list);
            mAdapter = new MusicListAdapter(getActivity(), mMusicList);
            mListView.setAdapter(mAdapter);
            mListView.setOnItemClickListener(this);
            mListView.setOnItemLongClickListener(this);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // TODO Auto-generated method stub
        mAdapter.onItemClick(position);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        mAdapter.onItemLongClick(position);
        return true;
    }

}
