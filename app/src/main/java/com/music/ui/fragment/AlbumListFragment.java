package com.music.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.music.R;
import com.music.ui.adapter.AlbumListAdapter;
import com.music.entity.AlbumInfo;
import com.music.sqlite.DBUtil;

import java.util.List;

/**
 * Created by dingfeng on 2016/4/15.
 */
public class AlbumListFragment extends FragmentBase implements AdapterView.OnItemClickListener {
    private Context mContext;
    private ListView mListView;
    private View mFooter;
    private TextView txtNum;
    private List<AlbumInfo> mAlbumList;
    private AlbumListAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.fragment_album_list, container, false);
        initView(view);
        bindData();
        return view;
    }

    private void initView(View view) {
        mListView = (ListView) view.findViewById(R.id.listView);
        mFooter = LayoutInflater.from(getContext()).inflate( R.layout.include_music_list_footer, null);
        txtNum = (TextView) mFooter.findViewById(R.id.txtNum);
        mListView.addFooterView(mFooter);
    }

    private void bindData() {
        mAlbumList = DBUtil.getInstance().getAlbumInfoDao().queryAll();
        if (mAlbumList != null) {
            mAdapter = new AlbumListAdapter(mContext, mAlbumList);
            mListView.setAdapter(mAdapter);
            mListView.setOnItemClickListener(this);
            txtNum.setText(getString(R.string.album_count, mAlbumList.size()));
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int postion, long id) {
        // TODO Auto-generated method stub
        AlbumInfo albuminfo = mAlbumList.get(postion);

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
//        transaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_from_left, R.anim.in_from_left, R.anim.out_from_right);
        transaction.setCustomAnimations(R.anim.in_from_right, 0, 0, R.anim.out_from_right);
        transaction.add(R.id.content, MusicListViewFragment.newInstance(albuminfo.getAlbumId(), MusicListViewFragment.FROM_ALBUM));
        transaction.hide(this);
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();
    }

}

