package com.music.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lzy.okserver.download.DownloadInfo;
import com.lzy.okserver.download.DownloadManager;
import com.lzy.okserver.download.DownloadService;
import com.music.R;
import com.music.ui.activity.DownloadActivity;
import com.music.ui.activity.LocalActivity;
import com.music.ui.activity.MusicListActivity;
import com.music.entity.MusicInfo;
import com.music.sqlite.DBUtil;

import java.util.List;

/**
 * Created by dingfeng on 2016/4/12.
 */
public class HomeFragment extends FragmentBase implements View.OnClickListener {

    private RelativeLayout rlLocal;
    private RelativeLayout rlDownload;
    private TextView txtLocalNum;
    private TextView txtDownloadNum;
    private RelativeLayout rlFavorite;
    private TextView txtFavoriteCount;

    private List<DownloadInfo> allTask;
    private DownloadManager downloadManager;
    private int downloadedMusic = 0;
    private int downloadingMusic = 0;
    private int favoriteCount = 0;
    private int localCount = 0;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            txtLocalNum.setText(getString(R.string.local_count, localCount));
            if (downloadingMusic > 0) {
                txtDownloadNum.setText(getString(R.string.downloading_count, downloadingMusic));
            } else {
                txtDownloadNum.setText(getString(R.string.downloaded_count, downloadedMusic));
            }
            txtFavoriteCount.setText(getString(R.string.favorite_count, favoriteCount));
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        initView(view);
        downloadManager = DownloadService.getDownloadManager();
        return view;
    }

    private void initView(View view) {
        rlLocal = (RelativeLayout) view.findViewById(R.id.rlLocal);
        rlDownload = (RelativeLayout) view.findViewById(R.id.rlDownload);
        txtLocalNum = (TextView) view.findViewById(R.id.txtLocalNum);
        txtDownloadNum = (TextView) view.findViewById(R.id.txtDownloadNum);
        rlLocal.setOnClickListener(this);
        rlDownload.setOnClickListener(this);
        rlFavorite = (RelativeLayout) view.findViewById(R.id.rlFavorite) ;
        txtFavoriteCount = (TextView) view.findViewById(R.id.txtFavoriteCount) ;
        rlFavorite.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlLocal:
                startActivity(new Intent(getContext(), LocalActivity.class));
                break;
            case R.id.rlDownload:
                startActivity(new Intent(getContext(), DownloadActivity.class));
                break;
            case R.id.rlFavorite:
                Intent intent = new Intent();
                intent.setClass(getContext(), MusicListActivity.class);
                intent.putExtra("fragment_type", 0);
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        getCount();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            getCount();
        }
    }

    public void getCount() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                // favorite
                List<MusicInfo> favorites = DBUtil.getInstance().getMusicInfoDao().queryList("isFavorite", true);
                if (favorites != null) {
                    favoriteCount = favorites.size();
                }
                // local
                localCount = (int)DBUtil.getInstance().getMusicInfoDao().count();
                // downloaded or downloading
                allTask = downloadManager.getAllTask();
                downloadedMusic = 0;
                for (DownloadInfo info : allTask) {
                    if (info.getState() == DownloadManager.FINISH) {
                        downloadedMusic ++;
                    }
                }
                downloadingMusic = allTask.size() - downloadedMusic;
                mHandler.sendEmptyMessage(0);
            }
        }).start();
    }

//    private void showFragment(Fragment fragment) {
//        FragmentTransaction transaction = getFragmentManager().beginTransaction();
//        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
//        transaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_from_left, R.anim.in_from_left, R.anim.out_from_right);
//        transaction.add(R.id.content, fragment);
//        transaction.hide(this);
//        transaction.addToBackStack(null);
//        transaction.commitAllowingStateLoss();
//    }

}
