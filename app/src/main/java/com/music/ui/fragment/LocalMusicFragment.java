package com.music.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.music.R;
import com.music.ui.adapter.IndicatorFragmentAdapter;
import com.windy.fragmenttab.TabInfo;
import com.windy.fragmenttab.TitleIndicator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dingfeng on 2016/9/25.
 */
public class LocalMusicFragment extends FragmentBase {

    private ViewPager mViewPager;
    private TitleIndicator mIndicator;
    private IndicatorFragmentAdapter mAdapter;
    private int mCurrentTab = 0;
    private int mLastTab = -1;
    private ArrayList<TabInfo> mTabs = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_download, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        mCurrentTab = supplyTabs(mTabs);
        mAdapter = new IndicatorFragmentAdapter(getContext(), getFragmentManager(), mTabs);
        mViewPager = (ViewPager) view.findViewById(R.id.viewPager);
        mViewPager.setAdapter(mAdapter);
        mViewPager.addOnPageChangeListener(onPageChangeListener);
        mViewPager.setOffscreenPageLimit(mTabs.size());
        mIndicator = (TitleIndicator) view.findViewById(R.id.pagerIndicator);
        mIndicator.init(mCurrentTab, mTabs, mViewPager);
        mViewPager.setCurrentItem(mCurrentTab);
        mLastTab = mCurrentTab;
    }

    public static final int FRAGMENT_MUSIC_LIST = 0;
    public static final int FRAGMENT_ARTIST_LIST = 1;
    public static final int FRAGMENT_ALBUM_LIST = 0;
    public static final int FRAGMENT_FOLDER_LIST = 1;

    private int supplyTabs(List<TabInfo> tabs) {
        tabs.add(new TabInfo(FRAGMENT_MUSIC_LIST, getString(R.string.fragment_downloaded),
                MusicListFragment.class));
        tabs.add(new TabInfo(FRAGMENT_ARTIST_LIST, getString(R.string.fragment_downloading),
                ArtistFragment.class));
        tabs.add(new TabInfo(FRAGMENT_ALBUM_LIST, getString(R.string.fragment_downloaded),
                AlbumListFragment.class));
        tabs.add(new TabInfo(FRAGMENT_FOLDER_LIST, getString(R.string.fragment_downloading),
                FolderListFragment.class));

        return FRAGMENT_MUSIC_LIST;
    }

    ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            mIndicator.onScrolled((mViewPager.getWidth() + mViewPager.getPageMargin()) * position + positionOffsetPixels);
        }

        @Override
        public void onPageSelected(int position) {
            mIndicator.onSwitched(position);
            mCurrentTab = position;
        }

        @Override
        public void onPageScrollStateChanged(int state) {
            if (state == ViewPager.SCROLL_STATE_IDLE) {
                mLastTab = mCurrentTab;
            }
        }
    };

}
