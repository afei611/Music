package com.music.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.music.R;
import com.music.ui.adapter.FolderListAdapter;
import com.music.entity.FolderInfo;
import com.music.sqlite.DBUtil;

import java.util.List;

/**
 * Created by dingfeng on 2016/4/29.
 */
public class FolderListFragment extends FragmentBase implements AdapterView.OnItemClickListener {

    private ListView mListView;
    private View mFooter;
    private TextView txtNum;
    private List<FolderInfo> mFolderList;
    private FolderListAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_folder_list, container, false);
        initView(view);
        bindData();
        return view;
    }

    private void initView(View view) {
        mListView = (ListView) view.findViewById(R.id.listView);
        mFooter = LayoutInflater.from(getContext()).inflate( R.layout.include_music_list_footer, null);
        txtNum = (TextView) mFooter.findViewById(R.id.txtNum);
        mListView.addFooterView(mFooter);
        List<FolderInfo> list = DBUtil.getInstance().getFolderInfoDao().queryAll();
        if (list != null) {
            txtNum.setText(getString(R.string.folder_count, list.size()));
        }
    }

    private void bindData() {
        mFolderList = DBUtil.getInstance().getFolderInfoDao().queryAll();
        if (mFolderList != null) {
            mAdapter = new FolderListAdapter(getActivity(), mFolderList);
            mListView.setAdapter(mAdapter);
            mListView.setOnItemClickListener(this);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        FolderInfo folder = mFolderList.get(position);

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
//        transaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_from_left, R.anim.in_from_left, R.anim.out_from_right);
        transaction.setCustomAnimations(R.anim.in_from_right, 0, 0, R.anim.out_from_right);
        transaction.add(R.id.content, MusicListViewFragment.newInstance(folder.getId(), MusicListViewFragment.FROM_FOLDER));
        transaction.hide(this);
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();
    }
}
