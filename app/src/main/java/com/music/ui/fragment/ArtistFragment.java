package com.music.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.music.R;
import com.music.ui.adapter.ArtistListAdapter;
import com.music.entity.ArtistInfo;
import com.music.sqlite.DBUtil;

import java.util.List;

/**
 * Created by dingfeng on 2016/4/29.
 */
public class ArtistFragment extends FragmentBase implements AdapterView.OnItemClickListener {
    private ListView mListView;
    private View mFooter;
    private TextView txtNum;
    private List<ArtistInfo> mArtistList;
    private ArtistListAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_artist_list, container, false);
        initView(view);
        bindData();
        return view;
    }

    private void initView(View view) {
        mListView = (ListView) view.findViewById(R.id.listview);
        mFooter = LayoutInflater.from(getContext()).inflate( R.layout.include_music_list_footer, null);
        txtNum = (TextView) mFooter.findViewById(R.id.txtNum);
        mListView.addFooterView(mFooter);
        List<ArtistInfo> list = DBUtil.getInstance().getArtistInfoDao().queryAll();
        if (list != null) {
            txtNum.setText(getString(R.string.artist_count, list.size()));
        }
    }

    private void bindData() {
        mArtistList = DBUtil.getInstance().getArtistInfoDao().queryAll();
        if (mArtistList != null) {
            mAdapter = new ArtistListAdapter(getActivity(), mArtistList);
            mListView.setAdapter(mAdapter);
            mListView.setOnItemClickListener(this);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // TODO Auto-generated method stub
        ArtistInfo artistInfo = mArtistList.get(position);

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
//        transaction.setCustomAnimations(R.anim.in_from_right, R.anim.out_from_left, R.anim.in_from_left, R.anim.out_from_right);
        transaction.setCustomAnimations(R.anim.in_from_right, 0, 0, R.anim.out_from_right);
        transaction.add(R.id.content, MusicListViewFragment.newInstance(artistInfo.artistId, MusicListViewFragment.FROM_ARTIST));
        transaction.hide(this);
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();
    }

}
