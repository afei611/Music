package com.music.ui.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.os.RemoteException;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.music.R;
import com.music.entity.MusicInfo;
import com.music.service.MusicUtil;
import com.music.sqlite.DBUtil;
import com.music.utils.StringUtil;

import java.util.List;

/**
 * Created by dingfeng on 2016/4/15.
 */
public class MusicListAdapter extends BaseListAdapter<MusicInfo> {

    public MusicListAdapter(Context context, List<MusicInfo> list) {
        super(context, list);
    }

    @Override
    public View bindView(int position, View convertView, ViewGroup parent) {
        final MusicInfo music = mList.get(position);
        ViewHolder viewholder;
        if (convertView == null) {
            viewholder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.music_list_item, null);
            viewholder.tv_music_name = (TextView) convertView.findViewById(R.id.tv_music_name);
            viewholder.tv_artist_name = (TextView) convertView.findViewById(R.id.tv_artist_name);
            viewholder.tv_music_duration = (TextView) convertView.findViewById(R.id.tv_music_duration);
            convertView.setTag(viewholder);
        } else {
            viewholder = (ViewHolder) convertView.getTag();
        }
        viewholder.tv_music_name.setText(music.getMusicName());
        viewholder.tv_artist_name.setText(music.getArtist());
        viewholder.tv_music_duration.setText(StringUtil.makeTimeString(music.getDuration()));

        return convertView;
    }

    class ViewHolder {
        TextView tv_music_name;
        TextView tv_artist_name;
        TextView tv_music_duration;
    }

    public void onItemClick(int position) {
        try {
            MusicUtil.sService.refreshPlayList(mList);
            MusicUtil.sService.play(position);
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void onItemLongClick(int position) {
        onLongClick(position);
    }

    private void onLongClick(int position) {
        final MusicInfo music = mList.get(position);
        AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
        dialog.setTitle(music.getMusicName());
        dialog.setItems(music.isFavorite ? R.array.music_dialog_collected : R.array.music_dialog, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        delete(music);
                        break;
                    case 1:
                        MusicUtil.setRingtone(mContext, music.songId, null);
                        break;
                    case 2:
                        setFavorite(music);
                        break;
                }
            }
        });
        dialog.show();
    }

    private void delete(final MusicInfo music) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext)
                .setMessage(mContext.getString(R.string.delete_music, music.getMusicName()))
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DBUtil.getInstance().getMusicInfoDao().delete(music);
                        mList.remove(music);
                        notifyDataSetChanged();
                        try {
                            MusicUtil.sService.removeMusic(music);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                        // update artist table
                        List<MusicInfo> list_artist = DBUtil.getInstance().getMusicInfoDao().queryList("artistId", music.artistId);
                        if (list_artist == null || list_artist.size() < 1) {
                            DBUtil.getInstance().getArtistInfoDao().delete("artistId", music.artistId);
                        } else {
                            DBUtil.getInstance().getArtistInfoDao().update("artistId", music.artistId, new String[]{"numOfTracks"}, new Object[]{list_artist.size()});
                        }
                        // update album table
                        List<MusicInfo> list_album = DBUtil.getInstance().getMusicInfoDao().queryList("albumId", music.albumId);
                        if (list_album == null || list_album.size() < 1) {
                            DBUtil.getInstance().getAlbumInfoDao().delete("albumId", music.albumId);
                        } else {
                            DBUtil.getInstance().getAlbumInfoDao().update("albumId", music.albumId, new String[]{"songsOfAlbum"}, new Object[]{list_album.size()});
                        }

                        Toast.makeText(mContext, mContext.getString(R.string.delete_music_success, music.getMusicName()), Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton(R.string.cancel, null);
        builder.show();
    }

    private void setFavorite(MusicInfo music) {
        music.setIsFavorite(!music.isFavorite);
        DBUtil.getInstance().getMusicInfoDao().update(music);
        Toast.makeText(mContext, music.isFavorite ?
                R.string.add_favorite_success : R.string.cancel_favorite_success,
                Toast.LENGTH_SHORT).show();
    }


}

