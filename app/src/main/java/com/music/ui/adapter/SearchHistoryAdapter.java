package com.music.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.music.R;
import com.music.entity.SearchHistory;
import com.music.sqlite.DBUtil;

import java.util.List;

/**
 * Created by dingfeng on 2016/9/30.
 */
public class SearchHistoryAdapter extends BaseListAdapter<SearchHistory> {

    public SearchHistoryAdapter(Context context, List<SearchHistory> list) {
        super(context, list);
    }

    @Override
    public View bindView(int position, View convertView, ViewGroup parent) {
        final SearchHistory item = mList.get(position);
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_search_history, null);
            holder = new ViewHolder();
            holder.txtKeyWord = (TextView) convertView.findViewById(R.id.txtKeyWord);
            holder.iv_remove = (ImageView) convertView.findViewById(R.id.iv_remove);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.txtKeyWord.setText(item.getKeyword());
        holder.iv_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDelete(item);
            }
        });

        return convertView;
    }

    class ViewHolder {
        TextView txtKeyWord;
        ImageView iv_remove;
    }

    public void onDelete(SearchHistory history) {
        mList.remove(history);
        notifyDataSetChanged();
        DBUtil.getInstance().getSearchDao().delete(history);
    }

}
