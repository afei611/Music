package com.music.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.music.R;
import com.music.entity.online.OnlineArtistInfo;

/**
 * Created by dingfeng on 2016/9/29.
 */
public class ArtistInfoActivity extends BaseActivity {
    private TextView txtTitle;
    private ImageView ivPic;
    private TextView txtName;
    private TextView txtCountry;
    private TextView txtBirth;
    private TextView txtConstellation;
    private TextView txtIntro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_info);
        initView();
        handleIntent(getIntent());
    }

    private void initView() {
        findViewById(R.id.back).setOnClickListener(backClickListener);
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        ivPic = (ImageView) findViewById(R.id.ivPic);
        txtName = (TextView) findViewById(R.id.txtName);
        txtCountry = (TextView) findViewById(R.id.txtCountry);
        txtBirth = (TextView) findViewById(R.id.txtBirth);
        txtConstellation = (TextView) findViewById(R.id.txtConstellation);
        txtIntro = (TextView) findViewById(R.id.txtIntro);
    }

    private void handleIntent(Intent intent) {
        OnlineArtistInfo onlineArtistInfo = (OnlineArtistInfo)intent.getSerializableExtra("artistInfo");
        String picUrl = onlineArtistInfo.getAvatar_s500();
        Glide.with(this)
                .load(picUrl)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(ivPic);

        String name = onlineArtistInfo.getName();
        txtTitle.setText(name);
        txtName.setText(getString(R.string.artist_name, name));

        String country = onlineArtistInfo.getCountry();
        txtCountry.setText(getString(R.string.artist_country, country));

        String birth = onlineArtistInfo.getBirth();
        txtBirth.setText(getString(R.string.artist_birth, birth));

        String constellation = onlineArtistInfo.getConstellation();
        txtConstellation.setText(getString(R.string.artist_constellation, constellation));

        String intro = onlineArtistInfo.getIntro();
        txtIntro.setText(getString(R.string.artist_intro, intro));
    }

}
