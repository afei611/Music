package com.music.ui.activity;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.music.IMediaPlaybackService;
import com.music.R;
import com.music.application.AppInfo;
import com.music.application.Constants;
import com.music.entity.AlbumInfo;
import com.music.entity.MusicInfo;
import com.music.service.MusicControl;
import com.music.service.MusicUtil;
import com.music.setting.ScanTask;
import com.music.setting.SensorListenerUtil;
import com.music.setting.Setting;
import com.music.sqlite.DBUtil;
import com.music.ui.adapter.IndicatorFragmentAdapter;
import com.music.ui.adapter.NowListAdapter;
import com.music.ui.fragment.BoardListFragment;
import com.music.ui.fragment.HomeFragment;
import com.music.utils.SharedPreferencesUtil;
import com.music.utils.StringUtil;
import com.windy.fragmenttab.TabInfo;
import com.windy.fragmenttab.TitleIndicator;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    private final static String TAG = MainActivity.class.getSimpleName();

    private DrawerLayout drawerLayout;
    private ImageView btSlide;
    private ImageView btSearch;
    private View bottom;
    private ImageView ivHead;
    private TextView txtName;
    private TextView txtArtist;
    private ImageButton btPlay;
    private ImageButton playList;
    private ProgressBar progressBar;

    private ViewPager mViewPager;
    private TitleIndicator mIndicator;
    private IndicatorFragmentAdapter mAdapter;
    private int mCurrentTab = 0;
    private ArrayList<TabInfo> mTabs = new ArrayList<>();

    private MusicUtil.ServiceToken mToken;
    private IMediaPlaybackService mService = null;

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            if (intent.getAction().equals(Constants.BROADCAST_THEMECHANGE)) {
//                setTheme();
            }
        }
    };

    private BroadcastReceiver mStatusListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(MusicControl.PLAYSTATE_CHANGED)) {
                mHandler.removeMessages(REFRESH_PROGRESS);
                mHandler.sendEmptyMessageDelayed(REFRESH_PROGRESS, 500);
                updateNowPlaying();
                setPauseButtonImage();
            } else if (action.equals(MusicControl.META_CHANGED)) {
                mHandler.removeMessages(REFRESH_PROGRESS);
                mHandler.sendEmptyMessageDelayed(REFRESH_PROGRESS, 500);
                updateNowPlaying();
                setPauseButtonImage();
            }
        }
    };

    public Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            switch (msg.what) {
                case REFRESH_PROGRESS:
                    refreshProgress();
                    mHandler.sendEmptyMessageDelayed(REFRESH_PROGRESS, 500);
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        // bind service
        mToken = MusicUtil.bindToService(this, serviceConnection);
        // create directory
        createDir();
        // first use
        if (SharedPreferencesUtil.getInstance().getShare("IsFirst", true)) {
            firstScan();
        }
        // register shake listener
        registerShakeEvent();
        // register broadcast receiver
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.BROADCAST_THEMECHANGE);
        registerReceiver(receiver, filter);

        IntentFilter f = new IntentFilter();
        f.addAction(MusicControl.PLAYSTATE_CHANGED);
        f.addAction(MusicControl.META_CHANGED);
        registerReceiver(mStatusListener, f);
    }

    private void initView() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        bottom = findViewById(R.id.bottom);
        bottom.setOnClickListener(this);
        ivHead = (ImageView) findViewById(R.id.ivHead);
        txtName = (TextView) findViewById(R.id.txtName);
        txtArtist = (TextView) findViewById(R.id.txtArtist);
        btPlay = (ImageButton) findViewById(R.id.btPlay);
        btPlay.setOnClickListener(this);
        playList = (ImageButton) findViewById(R.id.playList);
        playList.setOnClickListener(this);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
//        setTheme();
        btSlide = (ImageView) findViewById(R.id.btSlide);
        btSlide.setOnClickListener(this);
        btSearch = (ImageView) findViewById(R.id.btSearch);
        btSearch.setOnClickListener(this);
        // init fragment
        mCurrentTab = supplyTabs(mTabs);
        mAdapter = new IndicatorFragmentAdapter(this, getSupportFragmentManager(), mTabs);
        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        mViewPager.setAdapter(mAdapter);
        mViewPager.addOnPageChangeListener(onPageChangeListener);
        mViewPager.setOffscreenPageLimit(mTabs.size());
        mIndicator = (TitleIndicator) findViewById(R.id.pagerIndicator);
        mIndicator.init(mCurrentTab, mTabs, mViewPager);
        mViewPager.setCurrentItem(mCurrentTab);
    }

    private int supplyTabs(List<TabInfo> tabs) {
        tabs.add(new TabInfo(Constants.FRAGMENT_HOME, getString(R.string.fragment_home),
                HomeFragment.class));
        tabs.add(new TabInfo(Constants.FRAGMENT_ONLINE, getString(R.string.fragment_online),
                BoardListFragment.class));
        return Constants.FRAGMENT_HOME;
    }

    ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            mIndicator.onScrolled((mViewPager.getWidth() + mViewPager.getPageMargin()) * position
                    + positionOffsetPixels);
        }

        @Override
        public void onPageSelected(int position) {
            mIndicator.onSwitched(position);
            mCurrentTab = position;
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = IMediaPlaybackService.Stub.asInterface(service);
            setPauseButtonImage();
            try {
                updateNowPlaying();
                if (mService.isPlaying()) {
                    mHandler.sendEmptyMessageDelayed(REFRESH_PROGRESS, 500);
                }
            } catch (RemoteException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    private void firstScan() {
        new ScanTask(this, new ScanTask.Callback() {
            @Override
            public void start() {
                showLoadingProgressDialog();
            }

            @Override
            public void scan() {

            }

            @Override
            public void end(int result) {
                closeLoadingProgressDialog();
                try {
                    List<MusicInfo> list = DBUtil.getInstance().getMusicInfoDao().queryAll();
                    mService.refreshPlayList(list);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                updateNowPlaying();
                SharedPreferencesUtil.getInstance().putShare("IsFirst", false);
            }

        }).execute();
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
    }

    //register shake event
    private void registerShakeEvent() {
        SensorListenerUtil.getInstance(this).register(new SensorListenerUtil.ICallBack() {
            @Override
            public void onShake() {
                if (Setting.getInstance().mShakeChangeSong) {
                    next();
                }
            }

            @Override
            public void onProxityChange() {
                if (Setting.getInstance().mFlickChangeSong) {
                    next();
                }
            }
        });
    }

    private void next() {
        if (MusicUtil.sService != null) {
            try {
                mService.next();
            } catch (RemoteException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        mHandler.removeMessages(REFRESH_PROGRESS);
        unregisterReceiver(receiver);
        unregisterReceiver(mStatusListener);
        // unregister shake listener
        SensorListenerUtil.getInstance(this).unregister();
        MusicUtil.unbindFromService(mToken);
        mService = null;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // do not save state
    }

    @Override
    public void onClick(View view) {
        // TODO Auto-generated method stub
        switch (view.getId()) {
            case R.id.btSlide:
                openDrawer();
                break;
            case R.id.btSearch:
                startActivity(new Intent(MainActivity.this, SearchActivity.class));
                break;
            case R.id.bottom:
                try {
                    MusicInfo musicinfo = MusicUtil.sService.getCurrentMusic();
                    if (musicinfo == null) {
                        musicinfo = DBUtil.getInstance().getMusicInfoDao().queryFirst();
                    }
                    Intent intent = new Intent();
                    intent.setClass(MainActivity.this, MediaPlaybackActivity.class);
                    intent.putExtra("current", musicinfo);
                    startActivity(intent);
                } catch (RemoteException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
            case R.id.btPlay:
                doPauseResume();
                break;
            case R.id.playList:
                showNowPlayingList(view);
                break;
            default:
                break;
        }
    }

    private void doPauseResume() {
        try {
            if (mService != null) {
                if (mService.isPlaying()) {
                    mService.pause();
                } else {
                    mService.rePlay();
                }
                setPauseButtonImage();
            }
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void setPauseButtonImage() {
        try {
            if (mService != null && mService.isPlaying()) {
                btPlay.setImageResource(R.drawable.icon_pause_32);
            } else {
                btPlay.setImageResource(R.drawable.icon_play_32);
            }
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void updateNowPlaying() {
        try {
            MusicInfo musicinfo = mService.getCurrentMusic();
            if (musicinfo != null) {
                setCurrentInfo(musicinfo);
                refreshProgress();
            } else {
                setDefaultMusic();
            }
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /*
    * default music:the first music in locale music list
    * */
    private void setDefaultMusic() {
        List<MusicInfo> list = DBUtil.getInstance().getMusicInfoDao().queryAll();
        MusicInfo musicinfo;
        if (list != null && list.size() > 0) {
            musicinfo = list.get(0);
        } else {
            return;
        }
        setCurrentInfo(musicinfo);
    }

    private void setCurrentInfo(MusicInfo music) {
        txtName.setText(music.getMusicName());
        txtArtist.setText(music.getArtist());
        if (!StringUtil.isEmpty(music.getCover())) {
            Glide.with(this)
                    .load(music.getCover())
                    .placeholder(R.drawable.img_album_background)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(ivHead);
        } else {
            int albumId = music.getAlbumId();
            AlbumInfo albuminfo = DBUtil.getInstance().getAlbumInfoDao().query("albumid", albumId);
            if (albuminfo != null) {
                Glide.with(this)
                        .load(albuminfo.getAlbumCover())
                        .placeholder(R.drawable.img_album_background)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(ivHead);
            }
        }
    }

    private void refreshProgress() {
        if (mService == null) return;
        try {
            long duration = mService.duration();
            long position = mService.position();
            int rate = 0;
            if (duration != 0) {
                rate = (int) ((float) position / duration * 100);
            }
            progressBar.setProgress(rate);
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

//    private void setTheme() {
//        String path = SharedPreferencesUtil.getInstance().getShare(Constants.SELECTED_THEME, null);
//        Bitmap bitmap = getBitmapFromAssets(path);
//        if (bitmap != null) {
//            drawerLayout.setBackgroundDrawable(new BitmapDrawable(getResources(), bitmap));
//        }
//    }
//
//    private Bitmap getBitmapFromAssets(String path) {
//        AssetManager am = getAssets();
//        Bitmap bitmap = null;
//        try {
//            InputStream is = am.open("theme/" + path);
//            bitmap = BitmapFactory.decodeStream(is);
//            is.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return bitmap;
//    }

    private void createDir() {
        File root = new File(Constants.FbmusicRoot);
        if (!root.exists()) {
            root.mkdir();
        }
        File lyric = new File(Constants.LYRICPATH);
        if (!lyric.exists()) {
            lyric.mkdir();
        }
        File download = new File(Constants.DOWNLOAD);
        if (!download.exists()) {
            download.mkdir();
        }
    }

    public void openDrawer() {
        if (!drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerLayout.openDrawer(Gravity.LEFT);
        }
    }

    private long mLastBackTime = 0;
    private long mCurBackTime = 0;

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerLayout.closeDrawers();
            return;
        }
        mCurBackTime = System.currentTimeMillis();
        if (mCurBackTime - mLastBackTime > 2000) {
            Snackbar.make(txtName, R.string.prompt_exit, Snackbar.LENGTH_SHORT).show();
            mLastBackTime = mCurBackTime;
        } else {
            super.onBackPressed();
        }
    }

    private void showNowPlayingList(View view) {
        View contentView = LayoutInflater.from(this).inflate(R.layout.now_list, null);
        final PopupWindow popupWindow = new PopupWindow(contentView, LinearLayout.LayoutParams.MATCH_PARENT, AppInfo.screenHeight / 2, true);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        backgroundAlpha(0.5f);
        popupWindow.setAnimationStyle(R.style.popwin_anim_style);
        popupWindow.setOnDismissListener(new popDismissListener());

        int[] location = new int[2];
        view.getLocationOnScreen(location);
        popupWindow.showAtLocation(view, Gravity.NO_GRAVITY, location[0] - popupWindow.getWidth(), location[1] - popupWindow.getHeight());

        ListView listView = (ListView) contentView.findViewById(R.id.listview);
        try {
            List<MusicInfo> nowlist = mService.getPlayList();
            NowListAdapter adapter = new NowListAdapter(this, nowlist);
            listView.setAdapter(adapter);
            listView.setSelection(mService.getCurrentPos());
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        mService.play(position);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    popupWindow.dismiss();
                }
            });
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void backgroundAlpha(float bgAlpha) {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.alpha = bgAlpha;
        getWindow().setAttributes(lp);
    }

    class popDismissListener implements PopupWindow.OnDismissListener {

        @Override
        public void onDismiss() {
            // TODO Auto-generated method stub
            //Log.v("List_noteTypeActivity:", "我是关闭事件");
            backgroundAlpha(1.0f);
        }

    }

}
