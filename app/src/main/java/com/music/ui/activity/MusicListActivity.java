package com.music.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.music.R;
import com.music.ui.fragment.MusicListFragment;
import com.music.ui.fragment.FavoriteFragment;

/**
 * Created by dingfeng on 2016/9/22.
 */
public class MusicListActivity extends BaseActivity {

    private int fragmentType = -1;
    private MusicListFragment mMusicListFragment;
    private FavoriteFragment mFavoriteFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_list);
        handleIntent(getIntent());
    }

    private void handleIntent(Intent intent) {
        if (intent != null) {
            fragmentType = intent.getIntExtra("fragment_type", -1);
            showFragment(fragmentType);
        }
    }

    private void showFragment(int type) {
        if (type == -1) {
            if (mMusicListFragment == null) {
                mMusicListFragment = new MusicListFragment();
            }
            showFragment(mMusicListFragment);
        }
        if (type == 0) {
            if (mFavoriteFragment == null) {
                mFavoriteFragment = new FavoriteFragment();
            }
            showFragment(mFavoriteFragment);
        }
    }

    private void showFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.content, fragment);
        transaction.commitAllowingStateLoss();
    }

}
