package com.music.ui.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.music.R;
import com.music.ui.adapter.IndicatorFragmentAdapter;
import com.music.application.Constants;
import com.music.ui.fragment.AlbumListFragment;
import com.music.ui.fragment.ArtistFragment;
import com.music.ui.fragment.FolderListFragment;
import com.music.ui.fragment.MusicListFragment;
import com.windy.fragmenttab.TabInfo;
import com.windy.fragmenttab.TitleIndicator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dingfeng on 2016/9/25.
 */
public class LocalActivity extends BaseActivity {

    private RelativeLayout back;
    private TextView txtTitle;

    private ViewPager mViewPager;
    private TitleIndicator mIndicator;
    private IndicatorFragmentAdapter mAdapter;
    private int mCurrentTab = 0;
    private ArrayList<TabInfo> mTabs = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_local);
        back = (RelativeLayout) findViewById(R.id.back);
        back.setOnClickListener(backClickListener);
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtTitle.setText(R.string.local_music);
        // init fragment
        mCurrentTab = supplyTabs(mTabs);
        mAdapter = new IndicatorFragmentAdapter(this, getSupportFragmentManager(), mTabs);
        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        mViewPager.setAdapter(mAdapter);
        mViewPager.addOnPageChangeListener(onPageChangeListener);
        mViewPager.setOffscreenPageLimit(mTabs.size());
        mIndicator = (TitleIndicator) findViewById(R.id.pagerIndicator);
        mIndicator.init(mCurrentTab, mTabs, mViewPager);
        mViewPager.setCurrentItem(mCurrentTab);
    }

    private int supplyTabs(List<TabInfo> tabs) {
        tabs.add(new TabInfo(Constants.FRAGMENT_MUSIC_LIST, getString(R.string.fragment_music_list),
                MusicListFragment.class));
        tabs.add(new TabInfo(Constants.FRAGMENT_ARTIST_LIST, getString(R.string.fragment_artist_list),
                ArtistFragment.class));
        tabs.add(new TabInfo(Constants.FRAGMENT_ALBUM_LIST, getString(R.string.fragment_album_list),
                AlbumListFragment.class));
        tabs.add(new TabInfo(Constants.FRAGMENT_FOLDER_LIST, getString(R.string.fragment_folder_list),
                FolderListFragment.class));
        return Constants.FRAGMENT_MUSIC_LIST;
    }

    ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            mIndicator.onScrolled((mViewPager.getWidth() +
                    mViewPager.getPageMargin()) * position + positionOffsetPixels);
        }

        @Override
        public void onPageSelected(int position) {
            mIndicator.onSwitched(position);
            mCurrentTab = position;
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

}
