package com.music.entity.online;

import java.io.Serializable;

/**
 * Created by dingfeng on 2016/9/21.
 */
public class OnlineMusicInfo implements Serializable{

    private long artist_id;// 艺术家id
    private String pic_big;// 歌曲图像(大)
    private String pic_small;// 歌曲图像(小)
    private String publishtime;// 发布时间
    private String lrclink;// 歌词链接
    private String style;// 歌曲风格(影视原声、流行等等)
    private long song_id;// 歌曲id
    private String title;// 歌曲名
    private long ting_uid;//
    private String author;// 作者
    private String album_title;// 专辑
    private String artist_name;// 艺术家名

    public long getArtistId() {
        return artist_id;
    }

    public void setArtistId(long artist_id) {
        this.artist_id = artist_id;
    }

    public String getPicBig() {
        return pic_big;
    }

    public void setPicBig(String pic_big) {
        this.pic_big = pic_big;
    }

    public String getPicSmall() {
        return pic_small;
    }

    public void setPicSmall(String pic_small) {
        this.pic_small = pic_small;
    }

    public String getPublishTime() {
        return publishtime;
    }

    public void setPublishTime(String publishtime) {
        this.publishtime = publishtime;
    }

    public String getLrclink() {
        return lrclink;
    }

    public void setLrclink(String lrclink) {
        this.lrclink = lrclink;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public long getSongId() {
        return song_id;
    }

    public void setSongId(long song_id) {
        this.song_id = song_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getTingUid() {
        return ting_uid;
    }

    public void setTingUid(long ting_uid) {
        this.ting_uid = ting_uid;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAlbumTitle() {
        return album_title;
    }

    public void setAlbumTitle(String albumTitle) {
        this.album_title = albumTitle;
    }

    public String getArtistName() {
        return artist_name;
    }

    public void setArtistName(String artistName) {
        this.artist_name = artistName;
    }

}
