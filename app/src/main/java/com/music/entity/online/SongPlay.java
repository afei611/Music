package com.music.entity.online;

/**
 * Created by dingfeng on 2016/9/22.
 */
public class SongPlay {

    private Bitrate bitrate;

    public Bitrate getBitrate() {
        return bitrate;
    }

    public void setBitrate(Bitrate bitrate) {
        this.bitrate = bitrate;
    }

    public static class Bitrate {
        long song_file_id;
        String file_link;
        long file_size;
        String file_extension;
        String show_link;
        int file_duration;
        int file_bitrate;

        public long getSongFileId() {
            return song_file_id;
        }

        public void setSongFileId(long songFileId) {
            this.song_file_id = songFileId;
        }

        public String getFilelink() {
            return file_link;
        }

        public void setFileLink(String fileLink) {
            this.file_link = fileLink;
        }

        public long getFileSize() {
            return file_size;
        }

        public void setFileSize(long fileSize) {
            this.file_size = fileSize;
        }

        public String getExtension() {
            return file_extension;
        }

        public void setExtension(String extension) {
            this.file_extension = extension;
        }

        public String getShowLink() {
            return show_link;
        }

        public int getFileDuration() {
            return file_duration;
        }

        public void setFileDuration(int fileDuration) {
            this.file_duration = fileDuration;
        }

        public int getBitrate() {
            return file_bitrate;
        }

        public void setBitrate(int bitrate) {
            this.file_bitrate = bitrate;
        }

    }
}
