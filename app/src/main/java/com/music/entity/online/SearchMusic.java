package com.music.entity.online;

import java.util.List;

/**
 * Created by dingfeng on 2016/9/30.
 */
public class SearchMusic {

    private List<SearchSong> song;

    public List<SearchSong> getSong() {
        return song;
    }

    public void setSong(List<SearchSong> song) {
        this.song = song;
    }

    public static class SearchSong {
        String songname;
        String artistname;
        long songid;

        public String getSongName() {
            return songname;
        }

        public void setSongName(String songName) {
            this.songname = songName;
        }

        public String getArtistName() {
            return artistname;
        }

        public void setArtistName(String artistName) {
            this.artistname = artistName;
        }

        public long getSongId() {
            return songid;
        }

        public void setSongId(long songId) {
            this.songid = songId;
        }
    }
}
