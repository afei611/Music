package com.music.lrc;

/**
 * Created by dingfeng on 2016/10/11.
 */
public class LrcInfo {
    private String lrcContent;

    public String getLrcContent() {
        return lrcContent;
    }

    public void setLrcContent(String lrcContent) {
        this.lrcContent = lrcContent;
    }
}
